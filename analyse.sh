#! /bin/bash

export SOURCES=mapcontenders
export PROJECT=mapcontenders-qgis-plugin

sonar-scanner \
  -Dsonar.projectName="$PROJECT" \
  -Dsonar.projectKey="$PROJECT" \
  -Dsonar.projectVersion="$PLUGIN_VERSION" \
  -Dsonar.login="$SONAR_TOKEN" \
  -Dsonar.sources="$SOURCES" \
  -Dsonar.exclusions="$SOURCES/resources*.py" \
  -Dsonar.qualitygate.wait=true
