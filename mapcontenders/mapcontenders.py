import os.path
from typing import List

from PyQt5.QtWidgets import QToolBar
from qgis.gui import QgisInterface, QgsMapCanvas
from qgis.PyQt.QtCore import QCoreApplication, QSettings, QTranslator
from qgis.PyQt.QtWidgets import QAction

# Initialize Qt resources from file resources.py
from mapcontenders.resources import *  # noqa
from mapcontenders.utils import McLogger, get_icon

PLUGIN_NAME = "Map Contenders"
PLUGIN_NAME_SHORT = "MapContenders"


class MapContendersPlugin:

    iface: QgisInterface
    canvas: QgsMapCanvas
    actions: List[QAction]
    toolbar: QToolBar

    # constructor
    def __init__(self, iface: QgisInterface):

        # Create a logger
        self.logger = McLogger()
        # Save reference to the QGIS interface
        self.iface = iface
        self.canvas = iface.mapCanvas()
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        locale = QSettings().value("locale/userLocale")[0:2]
        locale_path = os.path.join(self.plugin_dir, "i18n", "MapContenders_{}.qm".format(locale))

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)
            QCoreApplication.installTranslator(self.translator)

        # Declare instance attributes
        self.actions = []
        self.toolbar = self.iface.addToolBar(PLUGIN_NAME)
        self.toolbar.setObjectName(PLUGIN_NAME)

    # noinspection PyMethodMayBeStatic
    def tr(self, message):
        return QCoreApplication.translate("MapContenders", message)

    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS GUI"""
        # pic2point
        pic2point_action = QAction(get_icon("geocamera"), "Pic 2 Point", self.iface.mainWindow())
        pic2point_action.triggered.connect(lambda _: self.logger.info("pic2point"))
        self.iface.addPluginToWebMenu(PLUGIN_NAME, pic2point_action)
        self.toolbar.addAction(pic2point_action)
        self.actions.append(pic2point_action)

        # point2pic
        point2pic_action = QAction(get_icon("location-filled"), "Point 2 Pic !", self.iface.mainWindow())
        point2pic_action.triggered.connect(lambda _: self.logger.info("point2pic"))
        self.iface.addPluginToWebMenu(PLUGIN_NAME, point2pic_action)
        self.toolbar.addAction(point2pic_action)
        self.actions.append(point2pic_action)

    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI"""
        for action in self.actions:
            self.iface.removePluginWebMenu(self.tr(PLUGIN_NAME), action)
            self.iface.removeToolBarIcon(action)
        del self.toolbar
