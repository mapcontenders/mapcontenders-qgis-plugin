__name__ = "mapcontenders"

from qgis.gui import QgisInterface


# noinspection PyPep8Naming
def classFactory(iface: QgisInterface):
    from mapcontenders.mapcontenders import MapContendersPlugin

    return MapContendersPlugin(iface)
